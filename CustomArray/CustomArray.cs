using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CustomArray
{
    public class CustomArray<T> : IEnumerable<T>
    {
        /// <summary>
        /// Should return first index of array
        /// </summary>
        public int First
        {
            get;
            private set;
        }

        /// <summary>
        /// Should return last index of array
        /// </summary>
        public int Last
        {
            get;
        }

        /// <summary>
        /// Should return length of array
        /// <exception cref="ArgumentException">Thrown when value was smaller than 0</exception>
        /// </summary>
        private int length;
        public int Length
        {
            get
            {
                return length;
            }
            private set
            {
                if (value <= 0) throw new ArgumentException("value is smaller than 0");
                length = value;
            }
        }

        /// <summary>
        /// Should return array 
        /// </summary>
        public T[] Array
        {
            get;
        }



        /// <summary>
        /// Constructor with first index and length
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="length">Length</param>      
        public CustomArray(int first, int length)
        {
            First = first;
            Length = length;
            Last = First + Length - 1;
            Array = new T[length];
        }

        /// <summary>
        /// Constructor with first index and collection  
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Collection</param>
        ///  <exception cref="NullReferenceException">Thrown when list is null</exception>
        /// <exception cref="ArgumentException">Thrown when count is smaler than 0</exception>       
        public CustomArray(int first, IEnumerable<T> list)
        {
            if (list is null)
            {
                throw new NullReferenceException("List is null");
            }
            if (list.Count() <= 0)
            {
                throw new ArgumentException("Count is smaller than 0");
            }
            this.First = first;
            Array = list.ToArray();
            Length = Array.Length;
            Last = First + Length - 1;
        }

        /// <summary>
        /// Constructor with first index and params
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Params</param>
        ///  <exception cref="ArgumentNullException">Thrown when list is null</exception>
        /// <exception cref="ArgumentException">Thrown when list without elements </exception>
        public CustomArray(int first, params T[] list)
        {
            if (list is null)
            {
                throw new ArgumentNullException(nameof(list), "List is null");
            }
            if (list.Length == 0)
            {
                throw new ArgumentException("List without elements");
            }
            First = first;
            Array = list;
            Length = Array.Length;
            Last = First + Length - 1;

        }

        /// <summary>
        /// Indexer with get and set  
        /// </summary>
        /// <param name="item">Int index</param>        
        /// <returns></returns>
        /// <exception cref="ArgumentException">Thrown when index out of array range</exception>  
        /// <exception cref="ArgumentNullException">Thrown in set  when value passed in indexer is null</exception>       
        public T this[int item]
        {
            get
            {
                if (item < First || item > Last)
                {
                    throw new ArgumentException("Index out of array range");
                }
                return Array[item - First];
            }
            set
            {
                if (item < First || item > Last)
                {
                    throw new ArgumentException("Index out of array range");
                }
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value), "Value passed in indexer is null");
                }
                Array[item - First] = value;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var item in Array)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
